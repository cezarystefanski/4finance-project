import React, { Component, PropTypes } from 'react';
import Header from '../components/Header';
import Shop from '../components/Shop';
import Basket from '../components/Basket';

class Container extends Component {
    render() {
        const { store, methods } = this.props;
        const basket = store.getState().basketOpened ? <Basket store={store} methods={methods} /> : null;

        return (
            <div className="app">
                {basket}
                <Header store={store} methods={methods} />
                <Shop store={store} methods={methods} />
            </div>
        )
    }
}

export default Container;
