import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import classes from './Sorter.scss';

class Sorter extends Component {
    constructor (props) {
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler() {
        let orderArr = this.refs.orderSelect.value.split('|');
        let { store, methods } = this.props.options;
        let [order, direction] = orderArr;
        store.dispatch(methods.changeOrder(order, direction));
        store.dispatch(methods.addToProducts());
        store.dispatch(methods.loadMoreProducts(true, true));
    }

    render() {
        return (
            <div>
                <h5>Sort:</h5>
                <select ref='orderSelect' onChange={this.changeHandler}>
                    <option value="DEFAULT|DESC">Default</option>
                    <option value="PRICE|DESC">By Price descending</option>
                    <option value="PRICE|ASC">By Price ascending</option>
                    <option value="BRAND|DESC">By Brand descending</option>
                    <option value="BRAND|ASC">By Brand ascending</option>
                </select>
            </div>
        );
    }
}

export default Sorter;