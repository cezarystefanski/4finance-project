import React, { Component } from 'react';
import Item from '../Item';
import classes from './Shop.scss';

class Shop extends Component {
    constructor(props) {
        super(props);
        this.loadMore = this.loadMore.bind(this);
    }
    loadMore() {
        let { store, methods } = this.props;
        store.dispatch(methods.loadMoreProducts());
    }
    componentDidMount() {
        const { store, methods } = this.props;
        const loadMore = this.refs.loadMore;

        document.addEventListener('scroll', () => {
            if (elementInViewport(loadMore)) {
                store.dispatch(methods.loadMoreProducts());
            }
        });

        function elementInViewport(el) {
            var top = el.offsetTop;
            var left = el.offsetLeft;
            var width = el.offsetWidth;
            var height = el.offsetHeight;

            while(el.offsetParent) {
                el = el.offsetParent;
                top += el.offsetTop;
                left += el.offsetLeft;
            }

            return (
                top < (window.pageYOffset + window.innerHeight) &&
                left < (window.pageXOffset + window.innerWidth) &&
                (top + height) > window.pageYOffset &&
                (left + width) > window.pageXOffset
            );
        }
    }
    render() {
        let { store, methods } = this.props;
        let prods = store.getState();
        let productElements = prods.currentView.map((product, idx) => <Item key={'item_' + idx} product={product} ref={'item_' + idx} store={store} methods={methods} />);
        let allItemsCount = store.getState().products.length;
        let viewCount = store.getState().currentView.length;
        let filterType = store.getState().filter;
        let loadMoreButton = (allItemsCount !== viewCount) && filterType === 'NORMAL' ? <button ref="loadMore" className="loadMore" onClick={this.loadMore}>Load more...</button> : null;

        return (
            <div>
                <ul>
                    {productElements}
                </ul>
                {loadMoreButton}
            </div>
        );
    }
}

export default Shop;