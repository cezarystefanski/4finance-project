import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { filters } from '../../store/actions';
import classes from './Filterer.scss';

class Filterer extends Component {
    constructor (props) {
        super(props);
        this.filterBrands = this.filterBrands.bind(this);
        this.filterCategories = this.filterCategories.bind(this);
        this.filterNormal = this.filterNormal.bind(this);
        this.toggleFilters = this.toggleFilters.bind(this);
        this.methods = props.options.methods;
        this.store = props.options.store;
    }

    filterBrands(e) {
        let clickedBrand = e.target.textContent;
        this.store.dispatch(this.methods.filterProds(clickedBrand, filters.BY_BRAND));
    }

    filterCategories(e) {
        let clickedCategory = e.target.textContent;
        this.store.dispatch(this.methods.filterProds(clickedCategory, filters.BY_TYPE));
    }

    filterNormal() {
        this.store.dispatch(this.methods.filterProds(null, filters.NORMAL));
    }

    toggleFilters() {
        this.store.dispatch(this.methods.toggleFilters());
    }

    render () {
        const { store } = this.props.options;
        const products = store.getState().products;
        let brandArr = products.reduce((prev, curr) => {
            if (prev.indexOf(curr.brand) === -1) {
                return prev.concat(curr.brand);
            }
            return prev;
        }, []);
        let categoryArr = products.reduce((prev, curr) => {
            if (prev.indexOf(curr.category) === -1) {
                return prev.concat(curr.category);
            }
            return prev;
        }, []);
        let brands = brandArr.map((brand, idx) => <li key={'brand_' + idx} ref={'brand_' + idx}><a className="filterer__option-link" onClick={this.filterBrands}>{brand}</a></li>);
        let categories = categoryArr.map((category, idx) => <li key={'category_' + idx} ref={'category_' + idx}><a className="filterer__option-link" onClick={this.filterCategories}>{category}</a></li>);
        let filters = (
            <div className="filterer">
                <div className="filterer__option">
                    <h4>by brand</h4>
                    <ul className="filterer__option-list">
                        {brands}
                    </ul>
                </div>
                <div className="filterer__option">
                    <h4>by category</h4>
                    <ul className="filterer__option-list">
                        {categories}
                    </ul>
                </div>
                <div className="filterer__option">
                    <h4 className="filterer__option-link" onClick={this.filterNormal}>clear filter</h4>
                </div>
            </div>
        );
        let filterSlider = store.getState().filtersOpen ? filters : null;

        return (
            <div className="Filterer">
                <h3 className="filterer__option-title">Filter by: <span className="filterer__openFilters" onClick={this.toggleFilters}>[+]</span></h3>
                {filterSlider}
            </div>
        );
    }
}

export default Filterer;