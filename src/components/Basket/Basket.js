import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './Basket.scss';
import BasketItem from '../BasketItem';

class Basket extends Component {
    constructor(props) {
        super(props);
        this.closeBasket = this.closeBasket.bind(this);
    }

    closeBasket() {
        let { store, methods } = this.props;
        store.dispatch(methods.closeBasket());
    }

    componentWillMount() {
        this.componentWillUpdate();
    }

    componentWillUpdate() {
        let { store } = this.props;
        this.basketArr = store.getState().basket;
        this.basket = this.basketArr.reduce((prev, curr) => {
            if (!prev.hasOwnProperty(curr.id)) {
                return Object.assign({}, prev, {
                    [curr.id]: {
                        item: curr,
                        amount: 1
                    }
                })
            } else {
                prev[curr.id].amount++;
                return prev;
            }
        }, {});
    }

    render() {
        let basketedProducts = Object.keys(this.basket);
        let basketItems = basketedProducts.map((item, idx) => <BasketItem key={'item_' + idx} ref={'item_' + item} className="basketLi" itemCode={item} basket={this.basket} options={this.props} />);

        return (
            <div className="basket__overlay">
                <h4 className="basket__overlay-close" onClick={this.closeBasket}>X</h4>
                <ul>
                    <li className="basket__overlay-legend">
                        <p>image</p>
                        <p>name</p>
                        <p>amount</p>
                        <p>remove one from basket</p>
                    </li>
                    {basketItems}
                </ul>
            </div>
        )
    }
}

export default Basket;