import React, {Component} from 'react';
import classes from './Details.scss';

class Details extends Component {
    constructor(props) {
        super(props);
        this.handleClose = this.handleClose.bind(this);
    }

    handleClose() {
        let {refs} = this;
        let {store, methods} = this.props;
        refs.detBox.classList.add('hidden');
        store.dispatch(methods.changeDescription(undefined));
    };

    render() {
        let state = this.props.store.getState();
        let description = state.detailsItem ? state.detailsItem.details : '';
        return (
        <div ref="detBox" className={"details hidden"}>
            <p className="close" ref="details__close" onClick={this.handleClose}>X</p>
            <p>{description}</p>
        </div>
        );
    }
}

export default Details;