import React, { Component } from 'react';
import classes from './Item.scss';
import Details from '../Details'

class Item extends Component {
    constructor(props) {
        super(props);
        this.handleDetails = this.handleDetails.bind(this);
        this.decreaseBasket = this.decreaseBasket.bind(this);
        this.increaseBasket = this.increaseBasket.bind(this);
        this.addThisItemToBasket = this.addThisItemToBasket.bind(this);
    }

    handleDetails() {
        let {store, methods, product} = this.props;
        this.refs.det.refs.detBox.classList.remove('hidden');
        store.dispatch(methods.changeDescription(product));
    }

    decreaseBasket() {
        this.refs.numberAdds.stepDown();
    }

    increaseBasket() {
        this.refs.numberAdds.stepUp();
    }

    addThisItemToBasket() {
        let { store, methods, product } = this.props;
        for (let i = 1; i <= this.refs.numberAdds.value; i++) {
            store.dispatch(methods.addToBasket(product));
        }
    }

    render() {
        let {product, store, methods} = this.props;
        return (
            <li className="itemWrap">
                <div className="shopItem">
                    <div className="prod">
                        <section className="info">
                            <img className="itemImg" src={product.image}/>
                            <div>
                                <h2 className="itemName">{product.name}</h2>
                                <small>{product.brand}</small>
                                <span className="viewDetails" onClick={this.handleDetails}>View Details</span>
                            </div>
                            <div className="discount">
                                <span>{product.discount * 100}%</span>
                                <small>Discount</small>
                            </div>
                        </section>
                        <section className="dets">
                            <div className="prices">
                                <span className="oldPrice">Old price: {product.price / 100}</span>
                                <span
                                    className="newPrice">Price: {((product.price - product.price * product.discount) / 100).toFixed(2)}</span>
                            </div>
                            <div className="item__dets-addCont">
                                <span onClick={this.decreaseBasket} className="baskCont">-</span>
                                <input ref="numberAdds" type="number" value="1" min="0" max="9" step="1" readOnly="readOnly" />
                                <span onClick={this.increaseBasket} className="baskCont">+</span>
                                <button className="item__dets-add2basket" onClick={this.addThisItemToBasket}>Add to Basket</button>
                            </div>
                        </section>
                    </div>
                    <progress value={product.availability} max="1">{product.availability * 100} %</progress>
                </div>
                <Details ref="det" store={store} methods={methods}/>
            </li>
        );
    }
}

export default Item;