import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Searcher extends Component {
    constructor (props) {
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler() {
        let search = this.refs.Searcher.value;
        let { store, methods } = this.props.options;
        store.dispatch(methods.searchFilter(search));
    }

    render() {
        return (
            <div>
                <h5>Search:</h5>
                <input type="text" className="searcher" name="Searcher" ref="Searcher" onChange={this.changeHandler}/>
            </div>
        );
    }
}

export default Searcher;