import React, { Component } from 'react';
import Sorter from '../Sorter';
import Searcher from '../Searcher';
import Filterer from '../Filterer';
import classes from './Header.scss'

class Header extends Component {
    constructor(props) {
        super(props);
        this.openBasket = this.openBasket.bind(this);
    }

    openBasket() {
        let { store, methods } = this.props;
        store.dispatch(methods.openBasket());
    }

    render() {
        let { store } = this.props;
        let basketCount = store.getState().basket.length;
        return (
            <header>
                <div className="header__topRow">
                    <h1 className="header__shopName"><span className="header__shopName-four">4</span>Shopping</h1>
                    <a className="header__basketLink" onClick={this.openBasket}>Basket ({basketCount})</a>
                </div>
                <div className="header__sorterRow">
                    <Sorter options={this.props} />
                    <Searcher options={this.props} />
                </div>
                <Filterer options={this.props} />
            </header>
        );
    }
}

export default Header;