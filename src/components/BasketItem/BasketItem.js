import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import styles from './BasketItem.scss'

class BasketItem extends Component {
    constructor(props) {
        super(props);
        this.removeInstancesFromBasket = this.removeInstancesFromBasket.bind(this);
    }

    removeInstancesFromBasket() {
        const { store, methods } = this.props.options;
        store.dispatch(methods.removeFromBasket(this.props.itemCode));
    }

    render() {
        let item = this.props.itemCode;
        return (<li className="basketItem">
            <img src={this.props.basket[item].item.image} />
            <p>{this.props.basket[item].item.name}</p>
            <p>{this.props.basket[item].amount}</p>
            <a className="basketItem__remove" onClick={this.removeInstancesFromBasket}>X</a>
        </li>);
    }
}

export default BasketItem;

