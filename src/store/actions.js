export const ADD_TO_BASKET = 'ADD_TO_BASKET';
export const SET_FILTER = 'SET_FILTER';
export const SHOW_DETAILS = 'SHOW_DETAILS';
export const ADD_TO_VIEW = 'ADD_TO_VIEW';
export const ADD_TO_CURRENT_VIEW = 'ADD_TO_CURRENT_VIEW';
export const ADD_FROM_INITIAL_TO_CURRENT_VIEW = 'ADD_FROM_INITIAL_TO_CURRENT_VIEW';
export const ADD_TO_PRODUCTS_ARRAY = 'ADD_TO_PRODUCTS_ARRAY';
export const LOAD_MORE_PRODUCTS = 'LOAD_MORE_PRODUCTS';
export const CHANGE_ORDER = 'CHANGE_ORDER';
export const CHANGE_DESCRIPTION = 'CHANGE_DESCRIPTION';
export const SEARCH = 'SEARCH';
export const FILTER_PRODS = 'FILTER_PRODS';
export const OPEN_BASKET = 'OPEN_BASKET';
export const CLOSE_BASKET = 'CLOSE_BASKET';
export const REMOVE_FROM_BASKET = 'REMOVE_FROM_BASKET';
export const TOGGLE_FILTERS = 'TOGGLE_FILTERS';
export const filters = {
    NORMAL: 'NORMAL',
    BY_BRAND: 'BY_BRAND',
    BY_TYPE: 'BY_TYPE'
};
export const order = {
    DEFAULT: 'DEFAULT',
    PRICE: 'PRICE',
    BRAND: 'BRAND'
};
export const orderDir = {
    DESC: 'DESC',
    ASC: 'ASC'
};

export function addToBasket(item) {
    return {
        type: ADD_TO_BASKET,
        item
    }
}

export function showDetails(item) {
    return {
        type: SHOW_DETAILS,
        item
    }
}

export function setFilter(filter) {
    return {
        type: SET_FILTER,
        filter
    }
}

export function addToView(items) {
    return {
        type: ADD_TO_VIEW,
        items
    }
}

export function addToCurrentView(currentProducts) {
    return {
        type: ADD_TO_CURRENT_VIEW,
        currentProducts
    }
}

export function addFromInitial() {
    return {
        type: ADD_FROM_INITIAL_TO_CURRENT_VIEW
    }
}

export function addToProducts() {
    return {
        type: ADD_TO_PRODUCTS_ARRAY
    }
}

export function loadMoreProducts(doKeep, reorder) {
    return {
        type: LOAD_MORE_PRODUCTS,
        doKeep,
        reorder
    }
}

export function changeOrder(orderType, direction) {
    return {
        type: CHANGE_ORDER,
        orderType,
        direction
    }
}

export function changeDescription(product) {
    return {
        type: CHANGE_DESCRIPTION,
        product
    }
}

export function searchFilter(searchString) {
    return {
        type: SEARCH,
        searchString
    }
}

export function filterProds(filterName, filterType) {
    return {
        type: FILTER_PRODS,
        filterName,
        filterType
    }
}

export function openBasket() {
    return {
        type: OPEN_BASKET
    }
}

export function closeBasket() {
    return {
        type: CLOSE_BASKET
    }
}

export function removeFromBasket(code) {
    return {
        type: REMOVE_FROM_BASKET,
        code
    }
}

export function toggleFilters() {
    return {
        type: TOGGLE_FILTERS
    }
}
