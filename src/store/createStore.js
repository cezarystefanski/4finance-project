import { createStore } from 'redux';
import shoppingApp from './reducers';

let store = createStore(shoppingApp);

export default store;