import { combineReducers } from 'redux';
import { searchFilter, filters, order, orderDir, SEARCH, SET_FILTER, ADD_TO_BASKET, ADD_TO_CURRENT_VIEW, ADD_FROM_INITIAL_TO_CURRENT_VIEW, ADD_TO_PRODUCTS_ARRAY, LOAD_MORE_PRODUCTS, CHANGE_ORDER, CHANGE_DESCRIPTION, FILTER_PRODS, OPEN_BASKET, CLOSE_BASKET, REMOVE_FROM_BASKET, TOGGLE_FILTERS } from './actions';
import * as allItems from '../data/index'

const initialState = {
    filter: filters.NORMAL,
    products: [],
    filteredProducts: [],
    basket: [],
    basketOpened: false,
    detailsItem: undefined,
    filterQuery: '',
    orderType: order.DEFAULT,
    orderDirection: orderDir.DESC,
    searchQuery: '',
    currentView: [],
    numInView: 0,
    filtersOpen: false,
    allItems
};

function shoppingApp(state = initialState, action) {
    switch (action.type) {
        case SET_FILTER:
            return Object.assign({}, state, {
                filter: action.filter
            });
        case ADD_TO_BASKET:
            return Object.assign({}, state, {
                basket: [
                    ...state.basket,
                    action.item
                ]
            });
        case ADD_TO_PRODUCTS_ARRAY:
            let itemsArray = Object.values(allItems.default);
            window.itemsArray = itemsArray;
            if (state.orderType === order.BRAND) {
                itemsArray = state.orderDirection === 'ASC' ? itemsArray.sort((a, b) => a.brand.localeCompare(b.brand)) : itemsArray.sort((a, b) => b.brand.localeCompare(a.brand));
            }
            if (state.orderType === order.PRICE) {
                itemsArray = state.orderDirection === 'ASC' ? itemsArray.sort((a, b) => (a.price - a.price * a.discount) - (b.price - b.price * b.discount)) : itemsArray.sort((a, b) => (b.price - b.price * b.discount) - (a.price - a.price * a.discount));
            }
            return Object.assign({}, state, {
                products: itemsArray
            });
        case LOAD_MORE_PRODUCTS:
            let numInView = state.numInView;
            let numNext;
            let view;
            if (!action.doKeep) {
                numNext = numInView <= state.products.length ? numInView + 6 : state.products.length;
            } else {
                numNext = numInView;
            }
            view = action.reorder ? state.products.slice(0, numInView) : state.currentView.concat([...state.products.slice(numInView, numNext)]);
            return Object.assign({}, state, {
                currentView: view,
                numInView: numNext
            });
        case CHANGE_ORDER:
            return Object.assign({}, state, {
                orderType: action.orderType,
                orderDirection: action.direction
            });
        case CHANGE_DESCRIPTION:
            return Object.assign({}, state, {
                detailsItem: action.product
            });
        case SEARCH:
            let searchedItems = state.products.filter(elem => {
                return elem.name.toLowerCase().indexOf(action.searchString.toLowerCase()) !== -1;
            });
            return Object.assign({}, state, {
                searchQuery: action.searchString,
                currentView: searchedItems
            });
        case FILTER_PRODS:
            let currentFilter = action.filterType;
            let currentName = action.filterName;
            let filteredView;
            if (currentFilter === filters.NORMAL) {
                filteredView = state.products;
            } else if (currentFilter === filters.BY_BRAND) {
                filteredView = state.products.filter(elem => elem.brand === currentName);
            } else if (currentFilter === filters.BY_TYPE) {
                filteredView = state.products.filter(elem => elem.category === currentName);
            }
            return Object.assign({}, state, {
                filter: currentFilter,
                filterQuery: currentName,
                currentView: filteredView
            });
        case OPEN_BASKET:
            return Object.assign({}, state, {
                basketOpened: true
            });
        case CLOSE_BASKET:
            return Object.assign({}, state, {
                basketOpened: false
            });
        case REMOVE_FROM_BASKET:
            let basket = state.basket;
            let itemNum = basket.findIndex(elem => elem.id === action.code);
            let newBasket = basket.filter((elem, idx) => idx !== itemNum);
            return Object.assign({}, state, {
                basket: newBasket
            });
        case TOGGLE_FILTERS:
            return Object.assign({}, state, {
                filtersOpen: !state.filtersOpen
            });
        default:
            return state;
    }
}

export default shoppingApp;