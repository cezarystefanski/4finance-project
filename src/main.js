import store from 'store/createStore';
import React from 'react';
import ReactDOM from 'react-dom';
import Container from './containers/Container.js'
import { addToBasket, showDetails, addToView, setFilter, addToCurrentView, addFromInitial, addToProducts, loadMoreProducts, changeOrder, changeDescription, searchFilter, toggleFilters, filterProds, openBasket, closeBasket, removeFromBasket } from 'store/actions';

const MOUNT_NODE = document.getElementById('main');
const methods = {
    loadMoreProducts,
    changeOrder,
    addToProducts,
    changeDescription,
    searchFilter,
    filterProds,
    addToBasket,
    openBasket,
    closeBasket,
    removeFromBasket,
    toggleFilters
};

let render = () => {
    ReactDOM.render(
        <Container store={store} methods={methods}/>,
        MOUNT_NODE
    );
};

store.dispatch(addToProducts());
store.subscribe(render);
store.dispatch(loadMoreProducts());
